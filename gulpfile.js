var gulp = require('gulp');
var inject = require('gulp-inject');
var wiredep = require('wiredep');

var wiredepOptions = {
  bowerJson: require('./bower.json'),
  directory: 'sinapi/static/vendor'
}

var js_files = [
  'sinapi/static/app/**/*.module.js',
  'sinapi/static/app/**/*.js'
]

var css_files = [
  'sinapi/static/css/**/*.css'
]

gulp.task('inject', function () {
  return gulp.src('sinapi/templates/base.html')
            .pipe(wiredep.stream(wiredepOptions))
            .pipe(inject(gulp.src(js_files)))
            .pipe(inject(gulp.src(css_files)))
            .pipe(gulp.dest('sinapi/templates/'));
});

gulp.task('inject-static', ['inject'], function () {
  var exec = require('child_process').exec;
  var command = './manage.py add_static_tags base.html --settings=sinapi.settings.development';
  exec(command, function (err, stdout, stderr) {
    if (err) {
      throw err;
    }

    console.log(stdout);
    console.log(stderr);
  });
});