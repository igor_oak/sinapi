"""Serializadores de registros."""

from rest_framework.serializers import CharField, ModelSerializer

from .models import Record


class RecordSerializer(ModelSerializer):

    """Serializa um registro do SINAPI."""

    type_choice = CharField(read_only=True, source='get_type_display')

    class Meta:
        model = Record
        fields = ['created', 'modified', 'code', 'description', 'unity', 'origin', 'price', 'month', 'year',
                  'type_choice']
