"""Testa o módulo pdf2db.converter."""

from unittest import TestCase as UnitTestCase

from django.test import TestCase

from ..models import Record
from ..pdf2db.converter import CompositionParser, InsumParser, save_pdf_to_db


class Converter2DBTest(TestCase):

    """Testes de conversão."""

    def test_pdf2db_converter_insum(self):
        """Verifica a função de conversão para insumos."""
        self.assertEqual(Record.objects.count(), 0)
        save_pdf_to_db('records/tests/insu.pdf', InsumParser(10, 2015))
        self.assertEqual(Record.objects.count(), 4993)

    def test_pdf2db_converter_comp(self):
        """Verifica a função de conversão para composições."""
        self.assertEqual(Record.objects.count(), 0)
        save_pdf_to_db('records/tests/comp.pdf', CompositionParser(10, 2015))
        self.assertEqual(Record.objects.count(), 4399)


class InsumParserTest(UnitTestCase):

    """Testa o parser de insumos."""

    def setUp(self):
        """setup."""
        self.parser = InsumParser(10, 2015)

    def test_make_record(self):
        """Testa se um registro é formatado corretamente."""
        record = [
            '00000055   ADAPTADOR DE COMPRESSAO EM POLIPROPILENO (PP), PARA TUBO EM '
            'PEAD, 20 MM X 1/2"     UN        AS                   1,81\n',
            '           - LIGACAO PREDIAL DE AGUA (NTS 179)\n',
            '  DE ACORDO COM A LEI\n'
        ]
        result = self.parser.make_record(record)
        expected = Record(
            code='00000055',
            description='ADAPTADOR DE COMPRESSAO EM POLIPROPILENO (PP), PARA TUBO EM PEAD, 20 MM X 1/2"'
            ' - LIGACAO PREDIAL DE AGUA (NTS 179) DE ACORDO COM A LEI',
            unity='UN',
            origin='AS',
            price='1,81',
            month=10,
            year=2015
        )
        self.assertEqual(result, expected)


class CompositionParserTest(UnitTestCase):

    """Testa o parser de composições."""

    def setUp(self):
        """setup."""
        self.parser = CompositionParser(10, 2015)

    def test_make_record(self):
        """Testa se um registro é formatado corretamente."""
        record = [
            '    73887/001 ASSENTAMENTO SIMPLES DE TUBOS DE FERRO FUNDIDO (FOFO) C/ JUNTA ELASTIC   M '
            '           CR                       2,54\n',
            '              A -   DN 75 MM - INCLUSIVE TRANSPORTE'
        ]
        result = self.parser.make_record(record)
        expected = Record(
            code='73887/001',
            description='ASSENTAMENTO SIMPLES DE TUBOS DE FERRO FUNDIDO (FOFO) C/ JUNTA ELASTIC'
            ' A -   DN 75 MM - INCLUSIVE TRANSPORTE',
            unity='M',
            origin='CR',
            price='2,54',
            month=10,
            year=2015,
        )
        self.assertEqual(result, expected)
