"""Formulários da aplicação registros."""

from django.forms import CharField, Form
from django.http import HttpResponse

from .models import Record
from .pdf2db.converter import save_records_to_excel


class ExportToExcelForm(Form):

    """Formulário responsável por gerar arquivos excel a partir de registros."""

    # conjunto de codes separados por vígula.
    query = CharField()
    sheet_name = CharField(required=False)

    def save(self):
        """Gera o conteúdo do arquivo excel."""
        query = self.cleaned_data['query'].split(',')
        sheet_name = self.cleaned_data['sheet_name'] or 'Planilha'
        response = HttpResponse(
            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        )
        response['Content-Disposition'] = 'attachment;filename="{}.xlsx"'.format(sheet_name)
        save_records_to_excel(Record.objects.filter(code__in=query), response)
        return response
