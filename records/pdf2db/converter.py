#!/usr/bin/env python3.5
"""Processador de arquivos sinapi."""

import re
from decimal import Decimal
from subprocess import PIPE, Popen

from io import StringIO
from xlsxwriter import Workbook

from ..models import Record


class InsumParser:

    """Gera registros a partir de arquivos de Insumos."""

    record_pattern = re.compile(r'(\d{8})\s+(.+?)\s+([\d\w/]+)\s+(\w+)\s+([\.\d]+,\d+)')
    start_space_pattern = re.compile(r'\s{11}[^\s]+')

    def __init__(self, month, year):
        """init."""
        self.month = month
        self.year = year
        self.type = 'insum'

    def make_record(self, buffer):
        """
        Cria um registro.

        Buffer é uma lista onde o primeiro elemento contém as informações do registro e os elementos
        seguintes, se houverem, são complementos da descrição desse registro.
        """
        # a primeira linha do registro é utilizada para gerar os dados principais. findall gera a lista
        # contendo os grupos capturados na expressão regular record_pattern.
        tokens = self.record_pattern.findall(buffer[0])[0]
        r = Record(
            code=tokens[0],
            description=tokens[1],
            unity=tokens[2],
            origin=tokens[3],
            price=Decimal(tokens[4].replace('.', '').replace(',', '.')),
            month=self.month,
            year=self.year,
            type=self.type
        )
        # nesse caso a descrição do registro foi quebrada em mais de uma linha no arquivo pdf.
        if len(buffer) > 1:
            # atualiza descrição desse registro com as informações restantes contidas no buffer.
            r.description = r.description + ' ' + ' '.join(map(lambda e: e.strip(), buffer[1:]))

        return r

    def parse_stream(self, stream):
        """Percorre o arquivo de texto filtrando registros."""
        # buffer que armazena informações para construir um registro.
        buffer = []
        # stream é um file-like object contendo dados para construir registros. Na prática é o arquivo
        # de texto convertido utilizando pdftotext.
        for line in stream:
            # nesse caso temos uma linha que corresponde a um registro, sendo portanto, adicionada ao buffer.
            if self.record_pattern.match(line):
                # se o buffer já contém dados de um registro, então precisamos gerá-lo e esvaziá-lo para
                # armazenar informações desse novo registro.
                if len(buffer) != 0:
                    yield self.make_record(buffer)
                    buffer = []

                buffer.append(line)

            # nesse caso o buffer já contém informações de um registro e informações extras a respeito da
            # descrição do mesmo existem. Acontece quando a descrição do registro no arquivo em pdf está
            # quebrada em multiplas linhas na célula de descrição.
            elif len(buffer) != 0 and self.start_space_pattern.match(line):
                buffer.append(line)

            # se nenhuma dessas condições for alcançada, a linha é descartada. Normalmente, isso corresponde
            # a linhas de texto comum do arquivo não relacionadas com registros.

        # se o stream já foi completamente percorrido e ainda existem dados no buffer, então só resta gerar
        # o último registro com os dados restantes.
        if len(buffer) != 0:
            yield self.make_record(buffer)


class CompositionParser(InsumParser):

    """Gera registros a partir de arquivos de Composições."""

    record_pattern = re.compile(r' +(\d{5}(/\d{3})?)\s+(.+?)\s+([\d\w/]+)\s+(\w+)\s+([\.\d]+,\d+)')
    start_space_pattern = re.compile(r'\s{14,16}[^\s]+')
    start_description_pattern = re.compile(r'\s+\d{4,5}\s+.+?')

    def __init__(self, month, year):
        """init."""
        super().__init__(month, year)
        self.type = 'composition'

    def make_record(self, buffer):
        """
        Cria um registro.

        O algoritmo é o mesmo em InsumParser.make_record.
        """
        tokens = self.record_pattern.findall(buffer[0])[0]
        r = Record(
            code=tokens[0],
            description=tokens[2],
            unity=tokens[3],
            origin=tokens[4],
            price=Decimal(tokens[5].replace('.', '').replace(',', '.')),
            month=self.month,
            year=self.year,
            type=self.type
        )
        if len(buffer) > 1:
            r.description = r.description + ' ' + ' '.join(map(lambda e: e.strip(), buffer[1:]))

        return r

    def parse_stream(self, stream):
        """
        Percorre o arquivo de texto filtrando registros.

        O algoritmo é o mesmo em InsumParser.parse_stream.
        """
        buffer = []
        for line in stream:
            if self.record_pattern.match(line):
                if len(buffer) != 0:
                    yield self.make_record(buffer)
                    buffer = []

                buffer.append(line)

            # a diferença é que um arquivo de composição contém mais um ponto de parada onde um registro pode
            # ser gerado. Acontece sempre que existe uma linha descrevendo um código mais geral de um item.
            elif len(buffer) != 0 and self.start_description_pattern.match(line):
                yield self.make_record(buffer)
                buffer = []

            elif len(buffer) != 0 and self.start_space_pattern.match(line):
                buffer.append(line)

        if len(buffer) != 0:
            yield self.make_record(buffer)


def save_pdf_to_db(pdf_file, parser):
    """Converte um arquivo sinapi em pdf para registros no banco de dados."""
    # pdftotext converte o arquivo pdf em texto e escreve o resultado no objeto file-like stream, na memória.
    stream = StringIO()
    with Popen(['pdftotext', '-layout', pdf_file, '-'], stdout=PIPE) as proc:
        stream.write(proc.stdout.read().decode('utf-8'))

    # o cursor do stream de dados é colocado no início para que esse stream possa ser processado por um
    # parser específico.
    stream.seek(0)
    Record.objects.bulk_create(parser.parse_stream(stream))


def save_records_to_excel(queryset, stream):
    """Escreve records definidos pelo queryset no stream de dados."""
    with Workbook(stream) as book:
        sheet = book.add_worksheet()
        money_format = book.add_format({'num_format': 'R$#,##0.00'})

        def get_with(i):
            """Obtem a largura da célula."""
            if i == 0:
                return 10

            if i == 1:
                return 120

            if i == 2:
                return 10

            if i == 3:
                return 15

            if i == 4:
                return 15

        for index, header in enumerate(['Código', 'Descrição', 'Unidade', 'Origem de Preço', 'Custo total']):
            sheet.set_column(index, index, get_with(index))
            sheet.write(0, index, header)

        for row, record in enumerate(queryset, 1):
            sheet.write(row, 0, record.code)
            sheet.write(row, 1, record.description)
            sheet.write(row, 2, record.unity)
            sheet.write(row, 3, record.origin)
            sheet.write(row, 4, record.price, money_format)
