"""Configuração de urls da aplicação registros."""

from django.conf.urls import url

from .views import CompositionSearchView, ExportToExcelView, InsumSearchView

urlpatterns = [
    url(r'^insum_search$', InsumSearchView.as_view(), name='insum_search'),
    url(r'^composition_search$', CompositionSearchView.as_view(), name='composition_search'),
    url(r'^export_to_excel', ExportToExcelView.as_view(), name='export_to_excel'),
]
