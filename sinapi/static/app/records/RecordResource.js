(function () {
  'use strict';

  angular.module('sinapi')
    .factory('RecordResource', RecordResource);

  function RecordResource($resource, recordAPIURL) {
    return $resource(recordAPIURL + ':id', {id: '@id'}, {
      update: {
        method: 'PUT'
      }
    });
  }
})();